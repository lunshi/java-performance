package fmkMalfunction;

public enum BusinessType {
    NONE_FLUENCE_ONE_TICKET("为影响一票货"),
    FLUENCE_ONE_TICKET("影响一票货"),
    NONE_FLUENCE_BUSINESS("为影响业务");
    String name;

    BusinessType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
