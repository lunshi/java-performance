package fmkMalfunction;

import java.util.Map;

/**
 * @author sl shilun217@qq.com
 * @Deacription
 * @date 2021/11/26/00:39
 **/
public class FmkMalfunctionNotify {
    Map<String, Employee> kushengNotifyPersonsMap;
    Map<String, Employee> voiceNotifyPersonsMap;
    Map<String, Employee> dealPersonsMap;
    boolean influenceOneTicket;
    boolean hadRestore;
    boolean createdGroup;
    String kushengMessage;
    String kushengTeamName;
    String currentNumber;
    BusinessType businessType;
    HandlerGrade handlerGrade;
    Map<String, FmkMalfunctionNotifyStepLog> fmkMalfunctionNotifyStepLogMap;

    public Map<String, Employee> getKushengNotifyPersonsMap() {
        return kushengNotifyPersonsMap;
    }

    public void setKushengNotifyPersonsMap(Map<String, Employee> kushengNotifyPersonsMap) {
        this.kushengNotifyPersonsMap = kushengNotifyPersonsMap;
    }

    public Map<String, Employee> getVoiceNotifyPersonsMap() {
        return voiceNotifyPersonsMap;
    }

    public void setVoiceNotifyPersonsMap(Map<String, Employee> voiceNotifyPersonsMap) {
        this.voiceNotifyPersonsMap = voiceNotifyPersonsMap;
    }

    public Map<String, Employee> getDealPersonsMap() {
        return dealPersonsMap;
    }

    public void setDealPersonsMap(Map<String, Employee> dealPersonsMap) {
        this.dealPersonsMap = dealPersonsMap;
    }

    public boolean isInfluenceOneTicket() {
        return influenceOneTicket;
    }

    public void setInfluenceOneTicket(boolean influenceOneTicket) {
        this.influenceOneTicket = influenceOneTicket;
    }

    public boolean isHadRestore() {
        return hadRestore;
    }

    public void setHadRestore(boolean hadRestore) {
        this.hadRestore = hadRestore;
    }

    public boolean isCreatedGroup() {
        return createdGroup;
    }

    public void setCreatedGroup(boolean createdGroup) {
        this.createdGroup = createdGroup;
    }

    public String getKushengMessage() {
        return kushengMessage;
    }

    public void setKushengMessage(String kushengMessage) {
        this.kushengMessage = kushengMessage;
    }

    public String getKushengTeamName() {
        return kushengTeamName;
    }

    public void setKushengTeamName(String kushengTeamName) {
        this.kushengTeamName = kushengTeamName;
    }

    public String getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(String currentNumber) {
        this.currentNumber = currentNumber;
    }

    public BusinessType getBusinessType() {
        return businessType;
    }

    public void setBusinessType(BusinessType businessType) {
        this.businessType = businessType;
    }

    public HandlerGrade getHandlerGrade() {
        return handlerGrade;
    }

    public void setHandlerGrade(HandlerGrade handlerGrade) {
        this.handlerGrade = handlerGrade;
    }

    public Map<String, FmkMalfunctionNotifyStepLog> getFmkMalfunctionNotifyStepLogMap() {
        return fmkMalfunctionNotifyStepLogMap;
    }

    public void setFmkMalfunctionNotifyStepLogMap(Map<String, FmkMalfunctionNotifyStepLog> fmkMalfunctionNotifyStepLogMap) {
        this.fmkMalfunctionNotifyStepLogMap = fmkMalfunctionNotifyStepLogMap;
    }
}
