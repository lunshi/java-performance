package fmkMalfunction;

/**
 * @author sl shilun217@qq.com
 * @Deacription
 * @date 2021/11/26/01:08
 **/
public class FmkMalfunctionNotifyKushengGroupPerson {
    String fmkMalfuntionNotifyId;
    String teamId;
    Long employeeId;
    String employeeNumber;
    String employeeName;
    String employeePhone;
    Boolean isAdd;
    Boolean status;

    public String getFmkMalfuntionNotifyId() {
        return fmkMalfuntionNotifyId;
    }

    public void setFmkMalfuntionNotifyId(String fmkMalfuntionNotifyId) {
        this.fmkMalfuntionNotifyId = fmkMalfuntionNotifyId;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeePhone() {
        return employeePhone;
    }

    public void setEmployeePhone(String employeePhone) {
        this.employeePhone = employeePhone;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
