package fmkMalfunction;

/**
 * @author sl shilun217@qq.com
 * @Deacription
 * @date 2021/11/26/00:15
 **/
public enum FmkMalfunctionNotifyStep {

    CREATE_KUSHENG_GROUP("创建跨声群"),
    ADD_MEMBERS_KUSHENG_GROUP("添加人员到跨声群"),
    SET_MANAGER_FOR_KUSHENG_GROUP("为跨声群设置管理员"),
    MODIFY_KUSHENG_GROUP_NAME("修改跨声群名称"),
    SEND_MESSAGE_TO_KUSHENG_GROUP("发送消息到跨声群"),
    SEND_NOTICE_TO_KUSHENG_GROUP("发送公告到跨声群"),
    SEND_MESSAGE_FIXED_KUSHENG_GROUP("发送消息到固定跨声群"),
    SEND_VOICE_NOTIFY_KUSHENG_GROUP("发送语音通知"),
    ;
    String name;

    FmkMalfunctionNotifyStep(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
