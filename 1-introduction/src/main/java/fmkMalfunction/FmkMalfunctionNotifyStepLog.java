package fmkMalfunction;

import java.util.Date;
import java.util.Map;

/**
 * @author sl shilun217@qq.com
 * @Deacription
 * @date 2021/11/26/00:15
 **/
public class FmkMalfunctionNotifyStepLog {
    FmkMalfunctionNotify fmkMalfunctionNotify;
    HandlerGrade handlerGrade;
    BusinessType businessType;
    FmkMalfunctionNotifyStep fmkMalfunctionNotifyStep;
    FmkMalfunctionNotifyStepStatus fmkMalfunctionNotifyStepStatus;
    Map<DataStatus,Object> dataMap;
    Date executeTime;
    String executeTimeStr;

    public HandlerGrade getHandlerGrade() {
        return handlerGrade;
    }

    public void setHandlerGrade(HandlerGrade handlerGrade) {
        this.handlerGrade = handlerGrade;
    }

    public BusinessType getBusinessType() {
        return businessType;
    }

    public void setBusinessType(BusinessType businessType) {
        this.businessType = businessType;
    }

    public FmkMalfunctionNotifyStep getFmkMalfunctionNotifyStep() {
        return fmkMalfunctionNotifyStep;
    }

    public void setFmkMalfunctionNotifyStep(FmkMalfunctionNotifyStep fmkMalfunctionNotifyStep) {
        this.fmkMalfunctionNotifyStep = fmkMalfunctionNotifyStep;
    }

    public FmkMalfunctionNotifyStepStatus getFmkMalfunctionNotifyStepStatus() {
        return fmkMalfunctionNotifyStepStatus;
    }

    public void setFmkMalfunctionNotifyStepStatus(FmkMalfunctionNotifyStepStatus fmkMalfunctionNotifyStepStatus) {
        this.fmkMalfunctionNotifyStepStatus = fmkMalfunctionNotifyStepStatus;
    }

    public Map<DataStatus, Object> getDataMap() {
        return dataMap;
    }

    public void setDataMap(Map<DataStatus, Object> dataMap) {
        this.dataMap = dataMap;
    }

    public Date getExecuteTime() {
        return executeTime;
    }

    public void setExecuteTime(Date executeTime) {
        this.executeTime = executeTime;
    }

    public String getExecuteTimeStr() {
        return executeTimeStr;
    }

    public void setExecuteTimeStr(String executeTimeStr) {
        this.executeTimeStr = executeTimeStr;
    }
}
