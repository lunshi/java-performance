package fmkMalfunction;

public enum FmkMalfunctionNotifyStepStatus {
    SUCCESS("成功",0),
    FAILED("失败",1),
    NOT_FAILED("部分失败",2);
    String name;
    Integer index;
    FmkMalfunctionNotifyStepStatus(String name,Integer index){
        this.index = index;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
