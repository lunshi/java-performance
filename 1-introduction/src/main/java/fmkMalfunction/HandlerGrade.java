package fmkMalfunction;

public enum HandlerGrade {
    NEW("新建"),
    UPGRADE("故障升级"),
    RESTORE("故障恢复");
    String name;

    HandlerGrade(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
